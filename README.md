This Patch is part of the [TESSER environment](https://bitbucket.org/AdrianArtacho/tesserakt/src/master/).

![TTESS:Logo](https://bitbucket.org/AdrianArtacho/tesserakt/raw/HEAD/TESSER_logo.png)

# Tesser_block

This device allows to block selectively different kinds of midi data.

![TESS:block](https://docs.google.com/drawings/d/e/2PACX-1vSlAh_LTPa2xUg6QhsWvAaiGfIpitpdf-YHpFK9PoYEO4NfnMeqil69FhrxB6wNfO3HG4V_xqAoVzzC/pub?w=405&h=219)

### Usage

The settings can be stored with the device/set. They can also be *automated*.

### Credits

This device is a derivative patch largely taken from AbletonDrummer's object.

____

# To-Do

* document device
* Add feature to **print in maxwindow** specific kinds of cc input
* Extended feature: block **specific notes which are set using the gesture CC**. This is practical, for example, if you are playing the keyboard and whant to block the area where the hands are from playing new notes, etc.
* 